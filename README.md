# A C++ ROS library for Vicon

## Contributors

* Emil Fresk

---

## License

Licensed under the LGPL-v3 license, see LICENSE file for details.

---

## Functionality

Publishes geometry messages, that are time-stamped, with the Vicon
measurements.
* Supports a "zero-pose" for calibrating an object.
